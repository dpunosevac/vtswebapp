(function () {
    'use strict';

    angular
        .module('vtsClient')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($modal, $state, toastr) {
        var vm = this;

        vm.pwChange = {};
        vm.newUser = {};

        var regModal = $modal({
            controllerAs: 'regModal',
            controller: 'LoginController',
            templateUrl: "app/login/regModal.html",
            show: false,
            backdrop: "static"
        });
        var pwModal = $modal({
            controllerAs: 'pwModal',
            controller: 'LoginController',
            templateUrl: "app/login/pwModal.html",
            show: false,
            backdrop: "static"
        });

        vm.openRegModal = function () {
            regModal.show();
        }

        vm.openPwModal = function () {
            pwModal.show();
        }

        vm.registrationSuccess = function () {
            toastr.success('Uspešno ste se registrovali');
        }

        vm.changePassSuccess = function () {
            toastr.success('Link je poslat na vaš e-mail');
        }

        vm.login = function () {
            $state.go('student');
            toastr.success('Uspešno ste se ulogovali');
        }
    }
})();
