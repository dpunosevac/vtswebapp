(function () {
  'use strict';

  angular
    .module('vtsClient', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'restangular',
      'ui.router',
      'mgcrea.ngStrap',
      'toastr',
      'ui.grid',
      'ui.grid.infiniteScroll'
    ]);
})();
