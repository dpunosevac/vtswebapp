(function () {
  'use strict';

  angular
    .module('vtsClient')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {
    $log.debug('runBlock start');
    $log.debug('test');
    $log.debug('runBlock end');
  }
})();
