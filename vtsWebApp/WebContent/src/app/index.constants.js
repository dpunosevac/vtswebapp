/* global moment:false */
(function () {
  'use strict';

  angular
    .module('vtsClient')
    .constant('moment', moment);
})();
