(function () {
    'use strict';

    angular
        .module('vtsClient')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('student', {
                url: '/student',
                templateUrl: 'app/student/student.html',
                controller: 'StudentController'
            })
            .state('test', {
                url: '/test',
                templateUrl: 'app/test/test.html',
                controller: 'TestController',
                controllerAs: 'Test'
            })
            .state('studentCrud', {
                url: '/student',
                abstract: true,
                template: '<ui-view />'
            })
            .state('studentCrud.edit', {
                url: '/edit/:id',
                templateUrl: 'app/student/crud/studentCrud.html',
                controller: 'StudentCrudController',
                controllerAs: 'studentCrud'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/login/login.html',
                controller: 'LoginController',
                controllerAs: 'login'
            });

        $urlRouterProvider.otherwise('/login');
    }
})();
