(function () {
    'use strict';

    angular
        .module('vtsClient')
        .controller('StudentController', StudentController);

    /** @ngInject */
    function StudentController($scope, $http, $timeout, $q, $state, $log, uiGridConstants, studentCrudService) {

        var myCellTemplate = '<div class="ui-grid-cell-contents">{{row.entity[col.field]}}</div>';
        var myRowTemplate = '<div ng-click="grid.appScope.openStudent(row.entity)" ng-repeat="col in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell studentGridRow" ui-grid-cell></div>';

        $scope.gridOptions = {
            enableFiltering: false,
            enableSorting: true,
            enableColumnMenus: false,
            enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
            rowTemplate: myRowTemplate,
            data: 'data',
            onRegisterApi: function (gridApi) {
                gridApi.infiniteScroll.on.needLoadMoreData($scope, $scope.getDataDown);
                gridApi.infiniteScroll.on.needLoadMoreDataTop($scope, $scope.getDataUp);
                $scope.gridApi = gridApi;
            },
            columnDefs: [
                {
                    field: 'index',
                    name: 'Br.Indexa',
                    width: '6%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'fin',
                    over: 'Fin.',
                    width: '4%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'ime',
                    name: 'Ime',
                    width: '14%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'prezime',
                    name: 'Prezime',
                    width: '14%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'semester',
                    name: 'Sem.',
                    width: '5%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'studGroup',
                    name: 'Studijska grupa',
                    width: '16%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'skolska',
                    name: 'Školska',
                    width: '8%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'dug',
                    name: 'Dug',
                    width: '12%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'pol',
                    name: 'Pol',
                    width: '4%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'ind',
                    name: 'Ind.',
                    width: '4%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'over',
                    name: 'Over.',
                    width: '5%',
                    cellTemplate: myCellTemplate
                },
                {
                    field: 'skolskaO',
                    name: 'ŠkolskaO',
                    width: '9%',
                    cellTemplate: myCellTemplate
                }
            ]
        }

        $scope.toggleFiltering = function () {
            $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        };

        $scope.data = [];

        $scope.firstPage = 0;
        $scope.lastPage = 0;

        $scope.getFirstData = function () {
            var promise = $q.defer();
            $http.get('assets/data/students.json')
                .success(function (data) {
                    var newData = $scope.getPage(data, $scope.lastPage);
                    $scope.data = $scope.data.concat(newData);
                    promise.resolve();
                });
            return promise.promise;
        };

        $scope.getDataDown = function () {
            var promise = $q.defer();
            $http.get('assets/data/students.json')
                .success(function (data) {
                    $scope.lastPage++;
                    var newData = $scope.getPage(data, $scope.lastPage);
                    $scope.gridApi.infiniteScroll.saveScrollPercentage();
                    $scope.data = $scope.data.concat(newData);
                    $scope.gridApi.infiniteScroll.dataLoaded($scope.firstPage > 0, $scope.lastPage < 4).then(function () { $scope.checkDataLength('up'); }).then(function () {
                        promise.resolve();
                    });
                })
                .error(function (error) {
                    $log.error(error);
                    $scope.gridApi.infiniteScroll.dataLoaded();
                    promise.reject();
                });
            return promise.promise;
        };

        $scope.getDataUp = function () {
            var promise = $q.defer();
            $http.get('assets/data/students.json')
                .success(function (data) {
                    $scope.firstPage--;
                    var newData = $scope.getPage(data, $scope.firstPage);
                    $scope.gridApi.infiniteScroll.saveScrollPercentage();
                    $scope.data = newData.concat($scope.data);
                    $scope.gridApi.infiniteScroll.dataLoaded($scope.firstPage > 0, $scope.lastPage < 4).then(function () { $scope.checkDataLength('down'); }).then(function () {
                        promise.resolve();
                    });
                })
                .error(function (error) {
                    $log.error(error);
                    $scope.gridApi.infiniteScroll.dataLoaded();
                    promise.reject();
                });
            return promise.promise;
        };


        $scope.getPage = function (data, page) {
            var res = [];
            for (var i = (page * 100); i < (page + 1) * 100 && i < data.length; ++i) {
                res.push(data[i]);
            }
            return res;
        };

        $scope.checkDataLength = function (discardDirection) {
            // work out whether we need to discard a page, if so discard from the direction passed in
            if ($scope.lastPage - $scope.firstPage > 3) {
                // we want to remove a page
                $scope.gridApi.infiniteScroll.saveScrollPercentage();

                if (discardDirection === 'up') {
                    $scope.data = $scope.data.slice(100);
                    $scope.firstPage++;
                    $timeout(function () {
                        // wait for grid to ingest data changes
                        $scope.gridApi.infiniteScroll.dataRemovedTop($scope.firstPage > 0, $scope.lastPage < 4);
                    });
                } else {
                    $scope.data = $scope.data.slice(0, 400);
                    $scope.lastPage--;
                    $timeout(function () {
                        // wait for grid to ingest data changes
                        $scope.gridApi.infiniteScroll.dataRemovedBottom($scope.firstPage > 0, $scope.lastPage < 4);
                    });
                }
            }
        };

        $scope.getFirstData().then(function () {
            $timeout(function () {
                // timeout needed to allow digest cycle to complete,and grid to finish ingesting the data
                // you need to call resetData once you've loaded your data if you want to enable scroll up,
                // it adjusts the scroll position down one pixel so that we can generate scroll up events
                $scope.gridApi.infiniteScroll.resetScroll($scope.firstPage > 0, $scope.lastPage < 4);
            });
        });

        $scope.openStudent = function (student) {
            studentCrudService.setStudent(student);
            $state.go("studentCrud.edit", { id: student.id });
        }
    }
})();
