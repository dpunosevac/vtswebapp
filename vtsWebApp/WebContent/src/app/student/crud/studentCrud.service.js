(function() {
  'use strict';

  angular
    .module('vtsClient')
    .service('studentCrudService', studentCrudService);

  /** @ngInject */
  function studentCrudService() {
    var vm = this;
    
    vm.student;
    vm.setStudent = setStudent;
    vm.getStudent = getStudent;
    
    function getStudent() {
      return vm.student;
    }
    
     function setStudent(student) {
      vm.student = student;
    }
  }
})();
