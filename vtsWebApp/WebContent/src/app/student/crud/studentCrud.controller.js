(function() {
    'use strict';

    angular
        .module('vtsClient')
        .controller('StudentCrudController', StudentCrudController);

    /** @ngInject */
    function StudentCrudController($state, studentCrudService) {
        var vm = this;
        
        vm.student = studentCrudService.getStudent();
        
        vm.goBack = function(){
            $state.go("student");
        }
    }
})();
