(function () {
    'use strict';

    angular
        .module('vtsClient')
        .directive('vtsNavbar', vtsNavbar);

    /** @ngInject */
    function vtsNavbar() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html',
            scope: {},
            controller: NavbarController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function NavbarController($state, toastr) {
            var vm = this;

            vm.tabs = ([
                { name: "Student", rel: "student" },
                { name: "Test", rel: "test" }
            ]);

            vm.isHeaderShown = function () {
                return $state.current.name !== '' && $state.current.name !== 'login';
            }

            vm.isTabActive = function (tabRel) {
                return $state.current.name.indexOf(tabRel) === 0;
            }

            vm.logout = function () {
                // Navigates to login state
                $state.go("login");
                toastr.success('Uspešno ste se izlogovali');
            }
        }
    }
})();
