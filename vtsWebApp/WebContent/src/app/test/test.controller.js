(function () {
    'use strict';

    angular
        .module('vtsClient')
        .controller('TestController', TestController);

    /** @ngInject */
    function TestController($log, toastr) {
        var vm = this;

        vm.testInfo = function () {
            toastr.info('Ovo je test info');
            $log.info("Ovo je test info")
        }

        vm.testWarn = function () {
            toastr.warning('Ovo je test warning');
            $log.warn("Ovo je test warning")
        }

        vm.testError = function () {
            toastr.error('Ovo je test error');
            $log.error("Ovo je test error")
        }

        vm.testInfo();
        vm.testWarn();
        vm.testError();
    }
})();
